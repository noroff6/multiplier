// Includes it's own header
#include "mul.h"

// The basic multiplier function
int mul(int x, int y)
{
    return x * y;
}