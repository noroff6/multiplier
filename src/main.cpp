// Author: Craig Marais
// Simple C++ program to display "Hello, World!" and use user created library

// Header file for input output functions
#include<iostream>
// Header file for the user library
#include "mul.h"

// main function -
// where the execution of program begins
int main()
{
    // prints hello world
    std::cout<<"Hello, World!"<<std::endl;
    //uses the multiplier library to find the answer
    std::cout<<"The answer is "<<mul(6,7)<<std::endl;

    return 0;
}